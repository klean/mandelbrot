
public class ComplexNum {
	
	protected double dReal = 0.0;
	protected double dImg = 0.0;
	
	public ComplexNum(double dRealIn,double dImgIn)
	{
		dReal = dRealIn;
		dImg = dImgIn;		
	}
	public double GetReal()
	{
		return dReal;
	}
	public double GetImg()
	{
		return dImg;
	}
	
	public ComplexNum Add(ComplexNum cnIn)
	{
		double dSumReal = this.GetReal() + cnIn.GetReal();
		double dSumImg = this.GetImg() + cnIn.GetImg();
		return new ComplexNum(dSumReal,dSumImg) ;
	}
	
	public ComplexNum Multiply(ComplexNum cnIn)
	{
		double dMulReal = (this.GetReal() * cnIn.GetReal())-(this.GetImg() * cnIn.GetImg());
		double dMulImg = (this.GetReal() * cnIn.GetImg())+(this.GetImg() * cnIn.GetReal());
		return new ComplexNum(dMulReal,dMulImg) ;
	}
}
