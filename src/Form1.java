import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.color.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;

public class Form1 extends JFrame {

	/**
	 * @param args
	 */
	MousePressListener MouseEventListner = new MousePressListener();
	int iClickX=0;
	int iClickY = 0;
	int iMagnify = 10; //edit this value to get magnification power
	
	
	public Form1()
	{
		add(jlPicBox1);
		addMouseListener(MouseEventListner);

	}
	
	double iFrameWidth = 1200;
	double iFrameHeight = 800;
	
	double dXAxisFrom = -2.5;
	double dXAxisTo = 1;
	double dYAxisFrom = -1;
	double dYAxisTo = 1;
	
	double dTotalXDistance = dXAxisTo -  dXAxisFrom;
	double dTotalYDistance = dYAxisTo -  dYAxisFrom;
	
	double dEachPxHeight = dTotalYDistance/iFrameHeight;
	double dEachPxWidth = dTotalXDistance/iFrameWidth;
	
	public static int iColorRGBJump = 20; //edit this value to get various colorscheme
	
	public static int iEachColorLength = 255/iColorRGBJump + 1;
	static Color[] cAllColors = new Color[iEachColorLength*iEachColorLength*iEachColorLength];

	double dCenterX = 1/dEachPxWidth*(Math.abs(dXAxisFrom));
	double dCenterY = 1/dEachPxHeight*(Math.abs(dYAxisFrom));
	
	public double GetXAxisFromAtCenterX(double dCenterXIn)
	{
		double result = (dCenterXIn * dEachPxWidth);
		return -result;
		
	}
	public double GetXAxisToAtCenterX(double dCenterXIn)
	{
		
		double result = dTotalXDistance + GetXAxisFromAtCenterX(dCenterXIn);
		return result;
	}
	public double GetYAxisFromAtCenterY(double dCenterYIn)
	{
		
		double result = (dCenterYIn * dEachPxHeight);
		return -result;
	}
	public double GetYAxisToAtCenterY(double dCenterYIn)
	{
		
		double result = dTotalYDistance + GetYAxisFromAtCenterY(dCenterYIn);
		return result;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	public double GetXForPixel(int iXIn)
	{
		return ((iXIn*dEachPxWidth)+dXAxisFrom);		
	}
	public double GetYForPixel(int iYIn)
	{
		return -1*(dYAxisFrom+(iYIn*dEachPxHeight));		
	}
	
	public double GetPixelForX(double dXIn)
	{
		return ((dXIn-dXAxisFrom)/dEachPxWidth);		
	}
	public double GetPixelForY(double dYIn)
	{
		return 500-((dYIn-dYAxisFrom)/dEachPxHeight);
	}
	public double DistanceFromOrigin(double dXIn,double dYIn)
	{
		return (dXIn*dXIn)+(dYIn*dYIn);	
	}
	
	
	public void paint (Graphics g) //overridden
	{
		
		
		System.out.println("Total x distance is " + dTotalXDistance);
		System.out.println("Total y distance is " + dTotalYDistance);
		
		System.out.println("Center is at ("+dCenterX+","+dCenterY+")");
		System.out.println("Each x axis pixel is "+dEachPxWidth+" units");
		System.out.println("Each y axis pixel is "+dEachPxHeight+" units");
		
		
		
		for(int x = 0; x <= iFrameWidth; x++)
		{
			for(int y = 0; y <= iFrameHeight; y++)
			{
				ComplexNum z0;
				ComplexNum zPrev;
				ComplexNum zCur;
				int iCounter = 0;
				
				double dCurX = GetXForPixel(x);
				double dCurY = GetYForPixel(y);			
				z0 = new ComplexNum(dCurX, dCurY);
				zPrev = z0;
				double dDistance = DistanceFromOrigin(dCurX,dCurY);
				
				while(iCounter < 1024 && dDistance <= 4)
				{
					zCur = zPrev.Multiply(zPrev).Add(z0);
					dDistance = DistanceFromOrigin(zCur.GetReal(),zCur.GetImg());
					zPrev = zCur;
					iCounter++;
				}
				
				if(iCounter < (iEachColorLength*iEachColorLength*iEachColorLength))
				{
					g.setColor(cAllColors[iCounter]);
					g.drawLine(x,y,x,y);
				}else
				{
					g.setColor(Color.red);
					g.drawLine(x,y,x,y);
				}

				
			}
		}
	}
	

	
	private JLabel jlPicBox1 = new JLabel(); //allows to put string graphics
	
	public static void main(String[] args) {
		GenerateAllColors();
		// TODO Auto-generated method stub
		Form1 frame1 = new Form1();
		frame1.setTitle("Drawing in Java");
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setSize(1200, 800);
		frame1.setVisible(true);
		Graphics paper = frame1.jlPicBox1.getGraphics();
		
				
		
		

	}
	static void GenerateAllColors()
	{
		int i = 0;
		for (int x = 0; x < 255;x+=iColorRGBJump)
		{
			for (int y = 0; y < 255;y+=iColorRGBJump)
			{
				for (int z = 0; z < 255;z+=iColorRGBJump)
				{
					cAllColors[i] = new Color(x,y,z);
					i++;
				}
			}
		}
	}
	
	class MousePressListener implements MouseListener{ //since its not extending, this class can be inside another class

		@Override
		public void mouseClicked(MouseEvent e) {
			
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
			iClickX = e.getX();
			iClickY= e.getY();
			
			System.out.println("Clicked on pixels " + iClickX + "," + iClickY);

			double iClickXPt = GetXForPixel(iClickX);
			double iClickYPt = GetYForPixel(iClickY);
			System.out.println("Clicked on points " + iClickXPt + "," + iClickYPt);
			
			double iXRange = 3.5/(1.1*iMagnify);
			double iYRange = 2/(1.1*iMagnify);;
			
			
			dXAxisFrom = (iClickXPt-(iXRange/2));
			dXAxisTo = (dXAxisFrom+iXRange);
			System.out.println("New X = "+dXAxisFrom+"->"+dXAxisTo);
			dYAxisFrom =-iClickYPt+(iYRange/2);
			dYAxisTo = -iYRange+dYAxisFrom;
			System.out.println("New Y = "+dYAxisFrom+"->"+dYAxisTo);


			dTotalXDistance = dXAxisTo -  dXAxisFrom;
			dTotalYDistance = dYAxisTo -  dYAxisFrom;
			
			
			
			dEachPxHeight = dTotalYDistance/iFrameHeight;
			dEachPxWidth = dTotalXDistance/iFrameWidth;

			dCenterX = 1/dEachPxWidth*(Math.abs(dXAxisFrom));
			dCenterY = 1/dEachPxHeight*(Math.abs(dYAxisFrom));
			
			

				
			 iMagnify++;
			
			repaint();
		}
		
		
	
	}

}



